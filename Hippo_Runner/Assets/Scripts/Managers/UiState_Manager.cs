using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiState_Manager : MonoBehaviour
{
    public static UiState_Manager S_UiStateInstance;

    
    [SerializeField]public static UIState currentState;
    
   private void Awake() {
        S_UiStateInstance = this;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateUIStateManager(UIState newstate)
    {
        currentState = newstate;
        UI_Manger.G_UIManager.UpdateUIState(newstate);
    }
}
