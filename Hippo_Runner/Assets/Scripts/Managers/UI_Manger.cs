using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_Manger : MonoBehaviour
{
    
    public static UI_Manger G_UIManager;
    [SerializeField] UIState State;

    public static event UnityAction<UIState> OnUIStateChange;
    [SerializeField]private List<GameObject> myUIGamePanels = new List<GameObject>();
    [SerializeField]private List<UIState> myUIStates = new List<UIState>();
    // Start is called before the first frame update
    private void Awake() {
        G_UIManager = this;
    }
    private void Start() {
        UpdateUIState(State);
    }

    public void UpdateUIState(UIState newState)
    {
        State = newState;
        SetUI(newState);

        switch (newState){
            case UIState.MainMenu:
            H_MainMenu();
            break;
            case UIState.Play:
            H_Play();          
            break;         
            case UIState.PauseMenu:
            H_Pause();
            break;  
            case UIState.Quit:
            H_Quit();
            break;  
            case UIState.OrderBuild:
            H_OrderBuild();
            break; 
            case UIState.Options:
            H_Options();
            break; 
            case UIState.Loading:
            H_Loading();
            break; 


        } 
        OnUIStateChange?.Invoke(newState);
    }


    private void SetUI(UIState value) 
    {
        for (int i = 0; i < myUIStates.Count; i++)
        {
           if (myUIStates[i] != value)
           {
            if (myUIGamePanels[i] != null)
            myUIGamePanels[i].SetActive(false);
           }else
           {
            if (myUIGamePanels[i] != null)
            myUIGamePanels[i].SetActive(true);
           }
        }
    }

    private void H_OrderBuild()
    {
        if (!CheckTime())
        ActivateGame();
    }

    private void H_Quit()
    {
        Application.Quit();
    }

    private void H_Pause()
    {   if (CheckTime())   
        DeactivateGame();      
    }

    private void H_Play()
    {
        if (!CheckTime())
        ActivateGame(); 
    }

    private void H_MainMenu()
    {
        if (CheckTime());
        DeactivateGame();
    }
    private void H_Loading()
    {
         if (CheckTime());
        DeactivateGame();
    }

    private void H_Options()
    {
         if (CheckTime());
        DeactivateGame();    
    }

    private void DeactivateGame()
    {
        Time.timeScale = 0;          
    }

     private void ActivateGame()
    {
        Time.timeScale = 1;
    }

    private bool CheckTime()
    {
        if (Time.timeScale == 1)
        {
            return true;
        }else
        {
            return false;
        }
    }

}

public enum UIState{
    MainMenu,
    PauseMenu,
    Play,
    Quit,
    Loading,
    Options,
    OrderBuild
}
